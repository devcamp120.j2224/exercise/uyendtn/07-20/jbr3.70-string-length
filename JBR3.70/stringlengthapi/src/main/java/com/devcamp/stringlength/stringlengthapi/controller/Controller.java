package com.devcamp.stringlength.stringlengthapi.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @CrossOrigin
    @GetMapping("length")
    public int stringLength() {
        String newString = "Hello devcamp120!";
        return newString.length();
    }
}
