package com.devcamp.stringlength.stringlengthapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StringlengthapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(StringlengthapiApplication.class, args);
	}

}
